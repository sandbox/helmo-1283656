<?php

/**
  * Before starting to delete the platform, make a backup
  *
  * Implements hook_pre_provision_delete()
  *
  * WARNING: This is a poor example of a method to create platform backups.
  * The future of this should probably make use of a dedicated platform backup comnmand, ideally based on 'drush archive-dump'
 */
function drush_backup_platform_pre_provision_delete($backup_file = NULL) {
  if (d()->type === 'platform') {
    $olddir = getcwd();
    if (!chdir(d()->root)) {
      return drush_set_error('PROVISION_BACKUP_PATH_NOT_FOUND', dt('cannot change directory to %dir', array('%dir' => d()->root)));
    }
    // same as above: some do not support -z
    $command = "tar cpf - . | gzip -c > %s";
    $name = preg_replace('/^@/', '', d()->name);
    $backup_file = d()->server->backup_path . '/' . $name . '-' . date("Ymd.His", mktime()) . '.tar.gz';

    $result = drush_shell_exec($command,  $backup_file);

    if (!$result && !drush_get_option('force', false)) {
      drush_set_error('PROVISION_BACKUP_FAILED', dt('Could not back up platform directory to @path', array('@path' => $backup_file), 'success'));
    } else {
      drush_log(dt('Backed up platform up to @path.', array('@path' => $backup_file), 'success'));
    }

    chdir($olddir);
  }
}

